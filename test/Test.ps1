$test_step_tag = 'integration'

$integration = Invoke-Pester . -tag $test_step_tag -PassThru
if ($integration.failedCount -gt 0) {
    Write-Error -ErrorAction stop "Integration failed, not continuing tests"
}
# if test_step_tag doesn't fail then run everything else.
Invoke-Pester . -ExcludeTag $test_step_tag

. (Join-Path $PSScriptRoot 'CONSTANTS.ps1')

Import-Module -force $MODULEPATH

Describe "Write-2LinePrompt" {
    $test_name = "Write-2LinePrompt"
    $testPath = "TestDrive:\$test_name"
    $test_location = new-item -ItemType Directory -Path $testPath

    BeforeEach {
        Push-Location $test_location
    }
    AfterEach {
        Pop-Location
    }

    it "String should be: <current_path> newline <current_folder> unicode space." {
        $correct = "$($test_location.fullname)`n$($test_location.name) λ "
        $result = Write-2LinePrompt
        $result | Should -BeExactly $correct
    }
    it "Changes when location does: subfolder to test location" {
        $name = 'example'
        New-Item -ItemType Directory -Name $name -OutVariable subtest |
        Push-Location

        $correct = "$($subtest.fullname)`n$($subtest.name) λ "
        $result = Write-2LinePrompt
        $result | Should -BeExactly $correct

        Pop-Location
    }

    Remove-Item -Recurse -Force -Path $test_location
}

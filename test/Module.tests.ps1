. (Join-Path $PSScriptRoot 'CONSTANTS.ps1')

$module = Import-Module -force $MODULEPATH -PassThru
$expected = Import-PowerShellDataFile $MODULEPATH

Describe "Module Loading Tests" -tag 'integration' {
    it "Functions to export should match" { $module.ExportedFunctions.keys | should -Be $expected.FunctionsToExport }
    it "Cmdlets to export should match" { $module.ExportedCmdlets.keys | should -Be $expected.CmdletsToExport }
    it "Variables to export should match" { $module.ExportedVariables.keys | should -Be $expected.VariablesToExport }
    it "Aliases to export should match" { $module.ExportedAliases.keys | should -Be $expected.AliasesToExport }
}

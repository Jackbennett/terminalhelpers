. (Join-Path $PSScriptRoot 'CONSTANTS.ps1')

Import-Module -force $MODULEPATH

Describe "Set-WindowTitle" {
    context "Expected Behaviour" {
        it "Called on it's own should do nothing" {
            $start = $host.ui.RawUI.WindowTitle
            Set-WindowTitle
            $host.ui.RawUI.WindowTitle | Should -Be $start
        }
        it "Set a simple title without a string" {
            $expected = "this is a title"
            Set-WindowTitle this is a title
            $host.ui.RawUI.WindowTitle | Should -Be $expected
        }
        it "Set a simple title with a string" {
            $customtitle = "this is another title"
            Set-WindowTitle "this is another title"
            $host.ui.RawUI.WindowTitle | Should -Be $customtitle
        }
    }
}

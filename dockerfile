FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS sdk
FROM mcr.microsoft.com/powershell:7.0.0-ubuntu-bionic AS pwsh

# Compile c# using .NET Core
FROM sdk AS build
WORKDIR /cmdlet
COPY ./src/csharp/*.csproj ./
RUN dotnet restore
COPY ./src/csharp/*.cs ./
RUN dotnet publish && mv ./bin/Debug/netcoreapp3.1/publish/*.dll ./
ENTRYPOINT [ "dotnet" ]
CMD ["publish"]

# Import tests and run pester at the specific powershell version published
FROM pwsh AS test
WORKDIR /cmdlet
RUN pwsh -command 'Install-Module Pester -Confirm:$false -force'
COPY --from=build ./cmdlet/*.dll ./src/
COPY ./test ./test
# Copy data, module and finally script files to source folder.
COPY [ "./src/*.psd1", "./src/*.psm1", "./src/*.ps1",  "./src/" ]
ENTRYPOINT [ "pwsh" ]
CMD ["-file", "./test/Test.ps1"]

# deploy from the .NET core which has nuget installed for publish-module.
FROM build AS deploy
# Deploy the As-tested module
COPY --from=test /cmdlet/src /usr/local/share/powershell/Modules/TerminalHelpers/
ENTRYPOINT [ "pwsh" ]

# Ready-to-use pwsh image to try the as deployed module with auto-loading. No tests or source.
FROM pwsh
WORKDIR /cmdlet
COPY --from=deploy /usr/local/share/powershell/Modules/TerminalHelpers/ /usr/local/share/powershell/Modules/TerminalHelpers/

ENTRYPOINT [ "pwsh" ]
CMD [ "-noExit", "-command", "get-command", "-Module", "TerminalHelpers" ]

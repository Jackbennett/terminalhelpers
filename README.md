Simple tools directly useful in the console. In general see each function help output.

![Example of prompt and title](./docs/examplePromptTitle.png)

*Background and Syntax colours are from Windows Terminal, prompt text hard-coded*

This README is a brief overview, for full docs use the powershell mechanisms `get-help`. Read on for developer/contributor information and plans.

# Write-SmartLineHistoryPrompt
Replacement prompt function to help divide the console history into per-directory sections.
This keeps your input closely left aligned to make it easy to scan through as you work.
Pays homage to cmder using the lambda symbol.

# Set-WindowTitle
Wraps setting `$host.ui.RawUI.WindowTitle` with all the function arguments because who as time to write a string in quotes.

```powershell
t this is all my title. Look ma no quotes!
```

# C# Progress

An experiment to find if there's a performance improvement to write prompt functions in C# but mainly a learning exercise for C# and a workflow.
Current state of `Write-2LinePrompt` functionally works but isn't built into the published module package yet.

See docker below this section or read on to build natively.
Clone the repo, run `dotnet build -i ./src` you will then be able to `invoke-pester` to start the unit tests or `import-module ./bin/Debug/netcoreapp3.1/TerminalHelpers.dll` directly.
NOTE: powershell cannot unload or replace an imported .dll, you will need to restart pwsh.exe to test rebuilt .dll.

Pipeline CI/CD should be written to compile c# for the same platforms .net core supports before publishing.

# Docker

Everything you need to try this module is in the below docker commands.

| Command                                                    | Description                                                                                                  |
| ---------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------ |
| `docker build -t tryme .`                                  | Makes a container with the module in the auto load path /cmdlet/terminalHelpers.                             |
| `docker run -it --rm tryme`                                | Starts pwsh with the module ready to go with help output. (removed once exited)                              |
| `docker build -t TerminalHelpers:test --target=test .`     | Source and tests are available in /cmdlet/src and test folders respectively with README.                     |
| `docker run -t --rm TerminalHelpers:test`                  | Runs the test from the built image                                                                           |
| `docker run --rm -it TerminalHelpers:test -noexit`         | Enter a pwsh session with all the source and tests to review or run.                                         |
| `docker build -t TerminalHelpers:deploy --target=deploy .` | Empty container except for the built module folder at the root. Planned to use for copying deployment files. |

# Workflow

See [gitlab push options](https://docs.gitlab.com/ee/user/project/push_options.html)

* `git push -o merge_request.create` to create a merge request of the current branch, this will auto-merge to master when tests pass.
* `git push -o ci.skip` skips CI, for example only a docs update.

Master will be kept working as development releases. Use branches for changes which will automatically merge once tests pass.

# Future Plans // Contributing

* PlatyPS for markdown -> online doc generation, Online docs hosted in gitlab pages so `get-help -online` works.
* As always, ever more pester tests
* Pester performance tests to track data for experiments
* Your own C# Prompts
* The gitlab CI flow needs improving to cache docker build and startup times.

**Contributions:** Please consider adding your own improvements to use this as an example/template for c# OR a workflow.

These are some over-arching goals or quick thoughts. See the project issues for fleshed out details.

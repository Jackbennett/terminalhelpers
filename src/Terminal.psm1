function Set-WindowTitle{
    <#
    .SYNOPSIS
        Anything passed to the function becomes the window title
    .DESCRIPTION
        All args are joined by a space so you don't even have to quote the window title as a string
    .EXAMPLE
        PS C:\> t this is a window title
        Sets the window title to "this is a window title"

        Recommended alias as t for speed (Set-Alias t Set-WindowTitle)
    .INPUTS
        String
    #>
    if($args){
        $host.ui.RawUI.WindowTitle = $args -join ' '
    }
}

function Set-WindowBackground {
    <#
    .SYNOPSIS
        Change background otherwise randomize it.
    .DESCRIPTION
        HSL colour string otherwise it will be a random compliment against the basic text colour to maintain contrast.

        Only applies to Windows Terminal app, other consoles don't expose the background like you expect, it's more like a highlight colour.
    .EXAMPLE
        PS C:\> Set-WindowBackground
        <random colour>

        Recommended alias as bg for speed (Set-Alias bg Set-WindowBackground)
    .INPUTS
        String
    #>
    write-error "You shouldn't be here. Waiting for windows terminal to add per-tab colour support (can only do per-profile)"

    # if windows terminal
        # Get settings .json
        # get font colour
        # generate background HSL
        # Set json
}

$script:lastPath
function Write-SmartLineHistoryPrompt{
    <#
    .SYNOPSIS
        Only show Current path on a new line when it's changed, otherwise print current folder.
    .EXAMPLE
        C:\
        src λ _
    #>
    $realLASTEXITCODE = $LASTEXITCODE
    $path = $pwd.ProviderPath
    $folder = Microsoft.PowerShell.Management\Split-Path $pwd.Path -Leaf

    if($script:lastPath -ne $path){ # Only print the full path on a new line if it changed
        $w = $host.ui.RawUI.MaxWindowSize.width
        $prettyline = "$path " + "-" * ($w - $path.length - 1)
        Microsoft.PowerShell.Utility\Write-Host $prettyline -ForegroundColor Cyan
    }

    Microsoft.PowerShell.Utility\Write-Host "$folder " -nonewline -ForegroundColor DarkGreen

    [char]0x03BB + " " # Must return something from the function to replace 'PS>'

    $script:lastPath = $path # Save our current path to test repetition
    $global:LASTEXITCODE = $realLASTEXITCODE # reset code in case a function called in prompt changed it.
}

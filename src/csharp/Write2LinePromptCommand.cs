using System;
using System.IO;
using System.Text;
using System.Management.Automation;

namespace TerminalHelpers
{
    [Cmdlet(VerbsCommunications.Write, "2LinePrompt")]
    [OutputType(typeof(string))]
    public class Write2LinePromptCommand : PSCmdlet
    {
        private string location
        {
            get => this.SessionState.Path.CurrentLocation.ProviderPath;
        }

        protected override void ProcessRecord()
        {
            // Setup the unicode output
            var unicode = new UnicodeEncoding();
            byte[] encodedBytes = unicode.GetBytes("\u03BB ");
            string[] breadcrumbs = location.Split(Path.DirectorySeparatorChar);
            Array.Reverse(breadcrumbs);

            WriteObject(
                string.Format("{0}\n{1} {2}", location, breadcrumbs[0], unicode.GetString(encodedBytes))
            );
        }
    }
}
